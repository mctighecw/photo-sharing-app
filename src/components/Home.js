import React from 'react';
import { Element, Events, animateScroll as scroll, scrollSpy, scroller } from 'react-scroll';

import Nav from 'Sections/Nav';
import About from 'Sections/About';
import Photos from 'Sections/Photos';
import Footer from 'Sections/Footer';

import info from 'Root/info.json';

class Home extends React.Component {
  constructor(props) {
    super(props);
  }

  scrollToTop() {
    scroll.scrollToTop();
  }

  scrollTo() {
    scroller.scrollTo('scroll-to-element', {
      duration: 800,
      delay: 0,
      smooth: 'easeInOutQuart'
    });
  }

  scrollToWithContainer() {
    let goToContainer = new Promise((resolve, reject) => {
      Events.scrollEvent.register('end', () => {
        resolve();
        Events.scrollEvent.remove('end');
      });

      scroller.scrollTo('scroll-container', {
        duration: 800,
        delay: 0,
        smooth: 'easeInOutQuart'
      });
    });

    goToContainer.then(() =>
      scroller.scrollTo('scroll-container-second-element', {
        duration: 800,
        delay: 0,
        smooth: 'easeInOutQuart',
        containerId: 'scroll-container'
      })
    );
  }

  handleScroll = (event) => {
    const navBar = document.getElementsByClassName('navbar')[0];

    if (window.pageYOffset > 60 ) {
      navBar.classList.add("navbar-scroll");
    }
    else {
      navBar.classList.remove("navbar-scroll");
    }
  };

  componentDidMount() {
    Events.scrollEvent.register('begin');
    Events.scrollEvent.register('end');
    window.addEventListener('scroll', this.handleScroll);
  }

  componentWillUnmount() {
    Events.scrollEvent.remove('begin');
    Events.scrollEvent.remove('end');
    window.removeEventListener('scroll', this.handleScroll);
  }

  render() {
    return (
      <div>
        <Nav info={info.nav} />
        <Element name="about"><About info={info.about} /></Element>
        <div className="hr" />
        <Element name="photos"><Photos info={info.photos} /></Element>
        <div className="hr" />
        <Footer />
      </div>
    )
  }
}

export default Home;
