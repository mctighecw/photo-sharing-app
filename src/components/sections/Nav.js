import React from 'react';
import { Navbar, Nav, NavDropdown, MenuItem, NavItem } from 'react-bootstrap';
import { Link } from 'react-scroll';

const NavBarComp = ({ info }) => (
  <Navbar collapseOnSelect fixedTop={true} fluid={true}>
    <Navbar.Header>
      <Navbar.Brand componentClass='span'>
        <Link to="about" spy={true} smooth={true} duration={500} offset={-90}>
          {info.app_name}
        </Link>
      </Navbar.Brand>
      <Navbar.Toggle />
    </Navbar.Header>
    <Navbar.Collapse>
      <Nav pullRight>
        <NavItem eventKey={2} componentClass='span'>
          <Link to="about" spy={true} smooth={true} duration={500} offset={-60}>
            {info.headings.about}
          </Link>
        </NavItem>
        <NavItem eventKey={2} componentClass='span'>
          <Link to="photos" spy={true} smooth={true} duration={500} offset={-120}>
            {info.headings.photos}
          </Link>
        </NavItem>
      </Nav>
    </Navbar.Collapse>
  </Navbar>
);

export default NavBarComp;
