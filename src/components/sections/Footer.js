import React from 'react';

const Footer = () => (
  <footer>
    <div className="container">
      <div className="copyright">
        &copy; 2018 Christian McTighe. Coded by Hand.
      </div>
    </div>
  </footer>
);

export default Footer;
