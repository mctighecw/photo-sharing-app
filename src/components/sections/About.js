import React from 'react';
import { Grid, Row, Col } from 'react-bootstrap';
import portrait from 'Assets/portrait.jpg';

const About = ({ info }) => (
  <Grid fluid={true}>
    <div className="about">
      <h1 className="section-heading">{info.section_name}</h1>

      <div className="info">
        <img src={portrait} alt="" />

        <div className="about-text">
          {info.paragraphs.length > 0 &&
            info.paragraphs.map((item, index) =>
              <p key={index}>{item}</p>
            )
          }
        </div>
      </div>
    </div>
  </Grid>
);

export default About;
