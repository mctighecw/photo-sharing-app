import React from 'react';
import fetch from 'isomorphic-fetch';
import { Dropbox } from 'dropbox';
import { Grid, Row, Col } from 'react-bootstrap';
import Pagination from 'Misc/Pagination';
import Modal from 'Misc/Modal';

const TOKEN = process.env.TOKEN;
const ERROR = 'Unable to get pictures from Dropbox';

class Photos extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      pics: [],
      showModal: false,
      modalPic: '',
      currentPage: 1,
      totalPics: '',
      picsPerPage: props.info.pics_per_page,
      error: '',
    };

    this.check = null;
  }

  getPictures = (start, end) => {
    return new Promise((resolve, reject) => {
      const dbx = new Dropbox({ accessToken: TOKEN, fetch: fetch });
      const self = this;
      let pics = [];

      dbx.filesListFolder({path: ''})
        .then(res => {
          const entries = res.entries;
          const sorted = entries.sort((a,b) => a.name - b.name);
          const selected = sorted.slice(start, end);

          this.setState({ totalPics: entries.length });

          selected.forEach(item => {
            self.getTemporaryLink(item.path_lower)
              .then(link => {
                const img = <img src={link} alt="" />;
                pics.push(img);
              })
              .catch(err => {
                this.setState({ error: ERROR });
                reject(err);
              });
          });

          this.check = setInterval(() => {
            if (selected.length === pics.length) {
              resolve(pics);
            }
          }, 200);
        })
        .catch(err => {
          this.setState({ error: ERROR });
          reject(err);
        });
    });
  }

  getTemporaryLink = (path) => {
    return new Promise((resolve, reject) => {
      const url = 'https://api.dropboxapi.com/2/files/get_temporary_link';
      const xhr = new XMLHttpRequest();

      xhr.open("POST", url, true);
      xhr.setRequestHeader("Content-Type", "application/json");
      xhr.setRequestHeader("Authorization", `Bearer ${TOKEN}`);
      xhr.send(JSON.stringify({ path }));

      xhr.onreadystatechange = () => {
        if (xhr.status === 200 && xhr.responseText) {
          const parsed = JSON.parse(xhr.responseText);
          const { link } = parsed;
          resolve(link);
        }
      }
    });
  }

  handleLoadSelectedPics = (start, end) => {
    this.getPictures(start, end).then(res => {
      this.setState({ pics: res });
      clearInterval(this.check);
    });
  }

  handleChangePage = (value) => {
    const { totalPics, currentPage, picsPerPage } = this.state;
    const totalPages = Math.ceil(totalPics / picsPerPage);
    const picStart = (currentPage - 1) * picsPerPage;
    const picEnd = currentPage * picsPerPage;

    if (value <= totalPages) {
      this.setState({ currentPage: value });
    }

    this.handleLoadSelectedPics(picStart, picEnd);
  }

  handleChangePicsPerPage = (event) => {
    const { picsPerPage, totalPics } = this.state;
    const totalPages = Math.ceil(totalPics / picsPerPage);
    const value = parseInt(event.target.value, 10);

    if (typeof value === 'number' && value > 0 && value <= totalPics) {
      this.setState({ currentPage: 1 }, () => {
        this.setState({ picsPerPage: value })
      });
    }
  }

  handleShowModal = (url) => {
    this.setState({ showModal: true, modalPic: url });
  }

  componentDidMount() {
    this.handleLoadSelectedPics(0, this.state.picsPerPage);
  }

  render() {
    const { pics, picsPerPage, currentPage, totalPics, showModal, modalPic, error } = this.state;
    const total = Math.ceil(totalPics / picsPerPage);
    const totalPages = total > 0 ? total : 1;

    return (
      <div>
        <Grid fluid={true}>
          <h1 className="section-heading">{this.props.info.section_name}</h1>

          <Row className="photos">
            {pics.length > 0 &&
              error === '' &&
              pics.map((pic, index) =>
                <Col xs={12} sm={6} md={4} lg={3} key={index} onClick={() => this.handleShowModal(pic.props.src)}>
                  <div className="pic-box">
                    {pic}
                  </div>
                </Col>
              )
            }
            {pics.length === 0 &&
              error === '' &&
              <div className="status-text">
                Loading...
              </div>
            }
            {error !== '' &&
              <div className="status-text">
                {error}
              </div>
            }
          </Row>
        </Grid>

        <Pagination
          picsPerPage={picsPerPage}
          currentPage={currentPage}
          totalPages={totalPages}
          handleChangePage={this.handleChangePage}
          handleChangePicsPerPage={this.handleChangePicsPerPage}
        />

        {showModal &&
          <Modal
            img={modalPic}
            close={() => this.setState({ showModal: false, modalPic: '' })}
          />
        }
    </div>
    )
  }
}

export default Photos;
