import React from 'react';
import UltimatePagination from 'react-ultimate-pagination-bootstrap-3';
import { FormControl } from 'react-bootstrap';

const Pagination = ({
    picsPerPage,
    currentPage,
    totalPages,
    handleChangePage,
    handleChangePicsPerPage
  }) => (
  <div>
    <div className="pagination-bar">
      <UltimatePagination
        currentPage={currentPage}
        totalPages={totalPages}
        onChange={handleChangePage}
      />
    </div>

    <div className="pics-per-page">
      <p>Pictures per page</p>
      <FormControl
        type="number"
        value={picsPerPage}
        placeholder=""
        onChange={handleChangePicsPerPage}
      />
    </div>
  </div>
);

export default Pagination;
