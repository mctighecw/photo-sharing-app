import React from 'react';
import { Modal, Button } from 'react-bootstrap';
import EXIF from 'exif-js';

class About extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      date: '',
    };
  }

  formatDate = (rawDate) => {
    if (rawDate) {
      const parsed = rawDate.split(" ");
      const date = parsed[0].split(":");
      const time = parsed[1].split(":");
      const formattedDate = `${date[1]}/${date[2]}/${date[0]}`;
      const formattedTime = `${time[0]}:${time[1]}`;
      return `${formattedDate} at ${formattedTime}`;
    }
  }

  getInfo = () => {
    const pic = document.getElementsByClassName('modal-pic')[0];
    const self = this;

    EXIF.getData(pic, function() {
      // const allData = EXIF.getAllTags(this);
      const tag = "DateTimeOriginal";
      const rawDate = EXIF.getTag(this, tag);
      const formatted = self.formatDate(rawDate);
      const date = formatted ? formatted : '';
      self.setState({ date });
    });
  }

  componentDidMount() {
    this.getInfo();
  }

  render() {
    const { img, close } = this.props;
    const { date } = this.state;
    const title = date === '' ? "Picture" : `Picture from ${date}`;

    return (
      <div className="static-modal">
        <Modal.Dialog>
          <Modal.Header>
            <Modal.Title>{title}</Modal.Title>
          </Modal.Header>

          <Modal.Body>
            <img src={img} className="modal-pic" alt="" />
          </Modal.Body>

          <Modal.Footer>
            <Button onClick={close}>
              Close
            </Button>
          </Modal.Footer>
        </Modal.Dialog>
      </div>
    )
  }
}

export default About;
