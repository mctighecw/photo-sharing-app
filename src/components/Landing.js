import React from 'react';
import { withRouter } from 'react-router-dom';

class Landing extends React.Component {
  componentDidMount() {
    setTimeout(() => this.props.history.push('/main'), 2000);
  }

  render() {
    return (
      <div className="landing-page">
        <div className="welcome-box">
          <h1>Welcome to Munich Impressions</h1>
          <h4>
            Loaded dynamically<br />
            from Dropbox
          </h4>
        </div>
      </div>
    )
  }
}

export default withRouter(Landing);
