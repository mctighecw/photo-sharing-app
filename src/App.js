import React from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';

import Landing from 'Components/Landing';
import Home from 'Components/Home';

import 'bootstrap-css-only/css/bootstrap.min.css';
import 'Styles/master.less';

const App = () => (
  <Switch>
    <Route exact path='/' component={Landing} />
    <Route path='/main' component={Home} />
    <Redirect to='/main' />
  </Switch>
);

export default App;
