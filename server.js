const path = require('path');
const express = require('express');
const app = express();

app.set('port', (process.env.PORT || 8000));
const port = app.get('port');

app.use(express.static(__dirname + '/build'));

app.get('/*', (req, res) => {
  res.sendFile(path.join(__dirname, '/build', 'index.html'));
});

app.listen(port, () => {
  console.log(`Server started at http://localhost:${port}`);
});
