# README

A photo sharing app that uses the Node.js API for Dropbox. Photos are loaded dynamically from a private Dropbox folder.

## App Information

App Name: photo-sharing-app

Created: Fall 2018

Creator: Christian McTighe

Email: mctighecw@gmail.com

Repository: [Link](https://gitlab.com/mctighecw/photo-sharing-app)

## Notes

### Tech Stack

- React
- React Router
- Webpack
- Bootstrap
- Less
- JavaScript Promises
- Node.js Express server
- Node.js API for Dropbox

## Credits

Favicon courtesy of [Flaticon.com](https://www.flaticon.com/authors/smashicons)

Last updated: 2025-02-21
